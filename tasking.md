# Tasking:

1. 创建Game静态组件
2. 创建PlayingPanel静态组件
3. Guess Card中输入猜测数字显示到Your Result中
4. 创建ControlPanel静态组件
5. ControlPanel中点击New Card生成答案并显示(答案保存在Game组件中)
6. ControlPanel中点击New Card清空PlayingPanel
7. ControlPanel中点击Show Result显示答案
8. Guess Card中点击Guess显示结果
