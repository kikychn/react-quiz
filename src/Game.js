import React, {Component} from 'react';
import PlayingPanel from "./PlayingPanel";
import ControlPanel from "./ControlPanel";

export default class Game extends Component{

  constructor(props, context) {
    super(props, context);

    this.handleAnswer = this.handleAnswer.bind(this);
    this.handleGuessResult = this.handleGuessResult.bind(this);

    this.state = {
      answer: '',
      guessResult: '',
    }
  }

  handleAnswer(value) {
    this.setState({
      answer: value
    }, () => {
      console.log(this.state.answer);
    });
  }

  handleGuessResult(value) {
    this.setState({
      guessResult: value
    });
  }

  render() {
    return (
      <div>
        <PlayingPanel answer={this.state.answer} guessResult={this.state.guessResult} handleGuessResult={this.handleGuessResult}></PlayingPanel>
        <ControlPanel handleAnswer={this.handleAnswer} answer={this.state.answer} handleGuessResult={this.handleGuessResult}></ControlPanel>
      </div>
    )
  }
}