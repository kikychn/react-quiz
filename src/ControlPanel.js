import React, {Component} from 'react';

export default class ControlPanel extends Component {

  constructor(props, context) {
    super(props, context);

    this.generateAnswer = this.generateAnswer.bind(this);
    this.showAnswer = this.showAnswer.bind(this);

    this.state = {
      answer: ''
    }
  }

  generateAnswer() {
    this.props.handleAnswer('');
    this.props.handleGuessResult('');
    var result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (let i = 0; i < 4; i++) {
      result += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    this.props.handleAnswer(result);
    this.setState(() => ({answer: result}));
    setTimeout(()=>this.setState(() => ({answer: ''})), 3000);
  }

  showAnswer() {
    this.setState({
      answer: this.props.answer
    })
  }

  render() {
    return (
      <div>
        <h1>New Card</h1>
        <input type="button" value='New Card' onClick={this.generateAnswer}/>
        <input type="text" value={this.state.answer.charAt(0)}/>
        <input type="text" value={this.state.answer.charAt(1)}/>
        <input type="text" value={this.state.answer.charAt(2)}/>
        <input type="text" value={this.state.answer.charAt(3)}/>
        <input type="button" value='Show Result' onClick={this.showAnswer}/>
      </div>
    )
  }
}