import React, {Component} from 'react';

export default class PlayingPanel extends Component {

  constructor(props, context) {
    super(props, context);

    this.changeNum = this.changeNum.bind(this);
    this.showSuccess = this.showSuccess.bind(this);

    this.state = {
      success: ''
    }
  }

  changeNum(event) {
    this.setState({success: ''});
    this.props.handleGuessResult(event.target.value);
  }

  showSuccess() {
    const tempSuccess = (this.props.guessResult === this.props.answer) ? 'SUCCESS' : 'FAILED';
    this.setState({success: tempSuccess});
  }

  render() {
    return (
      <section className='playingPanel'>
        <h1>Your Result</h1>
        <input type="text" value={this.props.guessResult.charAt(0)}/>
        <input type="text" value={this.props.guessResult.charAt(1)}/>
        <input type="text" value={this.props.guessResult.charAt(2)}/>
        <input type="text" value={this.props.guessResult.charAt(3)}/>
        <div>{this.state.success}</div>
        <h1>Guess Card</h1>
        <input type="text" onChange={this.changeNum} value={this.props.guessResult}/>
        <input type="button" value='Guess' onClick={this.showSuccess}/>
      </section>
    );
  }

}